#include <stdio.h>
#include <stdlib.h>

int main()
{
   static unsigned int counter = 0;
   static unsigned int temp = 0;
   const unsigned int KB = 1024;
   const unsigned int GB = KB * KB * KB;

   while (temp <= GB / KB) {
      if (malloc(KB) == NULL) {
         break;
      }
      counter++;
      if (counter % 1000 == 0) {
         temp += counter;
         printf("allocated MB: %i\n", temp / KB);
         counter = 0;
      }
   }
   printf("TOTAL MEMORY: %i MB\n", temp / KB);
   return EXIT_SUCCESS;
}