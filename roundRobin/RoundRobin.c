#include <stdio.h>
#include <Windows.h>

#define MAX_SIZE 100

int fib(int i);

int main()
{
   LARGE_INTEGER start, end, diff, frequence;
   LARGE_INTEGER array[MAX_SIZE];
   QueryPerformanceFrequency(&frequence);
   QueryPerformanceCounter(&start);

   for (int i = 0; i < MAX_SIZE; i++) {
      fib(35);
      QueryPerformanceCounter(&end);
      diff.QuadPart = end.QuadPart - start.QuadPart;
      array[i].QuadPart = diff.QuadPart;
   }

   for (int i = 0; i < MAX_SIZE; i++) {
      array[i].QuadPart = array[i].QuadPart * 1000000;
      array[i].QuadPart /= frequence.QuadPart;
      printf("%li\n", array[i].QuadPart);
   }
   return EXIT_SUCCESS;
}

int fib(int i)
{
   if (i == 0 || i == 1) {
      return 1;
   }
   return (fib(i - 1) + fib(i - 2));
}