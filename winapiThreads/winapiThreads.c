#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <conio.h>

#define THREAD_MAX 5

LARGE_INTEGER start, end, frequence;

int waitForThread = 1;

static int threadCounter = 0; // Seite 4/10 Punkt 1 Globale Variable 

struct threadParams
{
	int n;
	int tc;
	char* memory;
};

DWORD WINAPI myThread2(LPVOID lpParam);

int main(int argc, char *argv[])
{
	waitForThread = 1;
	printf("programm start...\n");

	char* p = malloc(1);
	(*p) = 'x';

	struct threadParams *params = (struct threadParams *) malloc(sizeof(struct threadParams) * THREAD_MAX);
	QueryPerformanceFrequency(&frequence);
	QueryPerformanceCounter(&start);

	for (int i = 0; i < THREAD_MAX; i++) {
		params[i].n = i + 1;
		QueryPerformanceCounter(&end);
		params[i].tc = (int)((end.QuadPart - start.QuadPart) * 1000000 / frequence.QuadPart);
		params[i].memory = p;
		CreateThread(NULL, 0, myThread2, (params + i), 0, NULL);
	}

	//wait until threads are finished

	while (waitForThread);
	printf("programm end...\n");
	_getch();
	return EXIT_SUCCESS;
}

DWORD WINAPI myThread2(LPVOID lpParam)
{
	LARGE_INTEGER end2;
	QueryPerformanceCounter(&end2);
	struct threadParams *localStruct = (struct threadParams *) lpParam;

	int startedAtTime = (int)((end2.QuadPart - start.QuadPart) * 1000000 / frequence.QuadPart); // // Seite 4/10 Punkt 3 eigene lokale Variable 
	printf("Thread %i created at %i and started at %i SameHeap: %p\n",
		localStruct->n,
		localStruct->tc,
		startedAtTime, localStruct->memory); // Seite 4/10 Punkt 1 denselben Heap
	Sleep(60000);
	printf("Thread %i terminating.\n", localStruct->n);
	threadCounter++;
	if (threadCounter == THREAD_MAX) {
		waitForThread = 0;
	}
}

DWORD WINAPI myThread1(LPVOID lpParam)
{
	printf("Hello World");
}