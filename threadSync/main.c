#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>
#include <conio.h>
#define MAX_LOOPS 100000

void umbuchungen(int* von, int* nach);
int randomAmount();
DWORD WINAPI thread_A(LPVOID lpParam);
DWORD WINAPI thread_B(LPVOID lpParam);
DWORD WINAPI thread_C(LPVOID lpParam);

CRITICAL_SECTION m_cs;
int konto1 = 1000000; // 1 Mio
int konto2 = 1000000; // 1 Mio

int main(void)
{
	
	printf("Start...\n");
	InitializeCriticalSection(&m_cs);
	CreateThread(NULL, 0, thread_A, (LPVOID)NULL, 0, NULL);
	CreateThread(NULL, 0, thread_B, (LPVOID)NULL, 0, NULL);
	CreateThread(NULL, 0, thread_C, (LPVOID)NULL, 0, NULL);
	
	Sleep(60000);
	printf("End...\n");
	_getch();
	return EXIT_SUCCESS;
}

DWORD WINAPI thread_A(LPVOID lpParam) // Konto 2 überweisung an Konto 1
{
	srand((unsigned int)time(NULL));
	for (;;)
	{
		umbuchungen(&konto2, &konto1);
		//printf("Thread_A:\nbetrag: %i\nkonto1: %i\nkonto2: %i\n", betrag, konto1, konto2);
	}
	printf("fertig A");
}

DWORD WINAPI thread_B(LPVOID lpParam) // Konto 1 überweisung an Konto 2
{
	srand((unsigned int)time(NULL));
	for (;;)
	{
		umbuchungen(&konto1, &konto2);
		//printf("Thread_A:\nbetrag: %i\nkonto1: %i\nkonto2: %i\n", betrag, konto1, konto2);
	}
	printf("fertig B");
}

DWORD WINAPI thread_C(LPVOID lpParam)
{
	for (size_t i = 0; i < MAX_LOOPS; i++)
	{
		printf("Thread_C:\nkonto1: %i\nkonto2: %i\nSumme: %i\n",
			konto1, konto2, konto1 + konto2);
		Sleep(1000);
	}
}

void umbuchungen(int* von, int* nach) {

	EnterCriticalSection(&m_cs);
	int betrag = randomAmount();
	int istStand1 = (*von);
	int istStand2 = (*nach);

	(*von) = istStand1 + betrag;
	(*nach) = istStand2 - betrag;

	LeaveCriticalSection(&m_cs);
}


int randomAmount()
{
	return (rand() % 100);
}

