#include <stdio.h>
#include <stdlib.h>

int main()
{
   printf("start writeToFile...\n");

   char ac[100];

   for (int i = 0; i < 100; ++i) {
      ac[i] = 'a';
   }

   FILE *pf;
   pf = fopen("e_a_write.txt", "w+");

   int counter = 0;
   while (counter != 1000000) {
      if (counter % 1000 == 0) {
         printf("%i records written:\n", counter);
      }
      fprintf(pf, "%s\n", ac);
      counter++;
   }
   fclose(pf);
   return EXIT_SUCCESS;
}